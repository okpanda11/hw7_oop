package task2;

public class Main {
    public static void main(String[] args) {
        Animals firstAnimal = Animals.AFRICAN_DOG;
        Animals secondAnimal = Animals.LION;
        Animals thirdAnimal = Animals.EAGLE;

        System.out.println(firstAnimal.toString()+"\n"+secondAnimal.toString()+"\n"+thirdAnimal.toString());
    }
}
