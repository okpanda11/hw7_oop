package task2;

public enum Animals {

LION(4), AFRICAN_DOG(2), GORILLA(3), EAGLE(5);

    int age;

    Animals(int age){
        this.age = age;
    }

    public int getAge(){
        return age;
    }

    @Override
    public String toString(){
        return "animal: "+super.toString()+", age: "+getAge();
    }
}
