package bonusTask;

public enum Vehicles {

    // elements
    TOYOTA(15000, "black"), MERCEDES(25000, "white"), KIA(12000, "blue"), MAZDA(14000, "black");
    int price;
    String color;

    // constructor
    Vehicles(int price, String color) {
        this.price = price;
        this.color = color;
    }

    // getters
    public int getPrice() {
        return price;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "model: " + super.toString() + ", price: " + getPrice() + ", color: " + getColor();
    }

}
